﻿using System;
using System.Buffers;
using System.Collections.Generic;
using System.Net.Http.Headers;
using System.Xml.Linq;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.Handler.Entities;


namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class XmlParser: IDataParser<List<Customer>>
    {
        private readonly List<string> _lines;
        public XmlParser(ref List<string> lines) 
        {
            _lines = lines;
        }
        public List<Customer> Parse()
        {
            List<Customer> result = new List<Customer>();
            //string rs = "";
            //foreach(var s in _lines) rs += s;
            //var xe = XElement.Parse(rs);
            //foreach(var re in xe.Elements())
            //{
            //    var cust = new Customer()
            //    {
            //        Id = Convert.ToInt32(re.Element("Id").Value),
            //        Phone = re.Element("Phone").Value ,
            //        Email = re.Element("Email").Value,
            //        FullName = re.Element("FullName").Value
            //    };
            //    result.Add(cust);
            //}
            //return result;
            Customer cs = null;
            foreach(var s in _lines)
            {
                string val = "";
                string tn = getTag(s.Trim(), out val);

                switch (tn)
                {
                    case "<Customer>":
                        {
                            cs = new Customer();
                            break;
                        }
                    case "<Id>":
                        {
                            cs.Id = Convert.ToInt32(val);
                            break;
                        }
                    case "<Phone>":
                        {
                            cs.Phone = val;
                            break;
                        }
                    case "<Email>":
                        {
                            cs.Email = val;
                            break;
                        }
                    case "<FullName>":
                        {
                            cs.FullName = val;
                            break;
                        }
                    case "</Customer>":
                        {
                            result.Add(cs);
                            break;
                        }
                    default: 
                        {
                            break;
                        }
                }
                
            }
            return result;

            string getTag(string s, out string val )
            {
                int i = 0;
                val = "";
                string sr = "";
                while (s[i] != '>') { sr += s[i]; i++; }
                sr += s[i]; // end quote
                i++;
                if(i >= s.Length) { return sr; }
                while (s[i] != '<') { val += s[i]; i++; }
                return sr;
            }
            
        }
    }
}
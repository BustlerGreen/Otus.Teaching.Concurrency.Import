﻿using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace Otus.Teaching.Concurrency.Import.Loader.Loaders
{
     
    public class XmlRangeLoader : IDataLoader
    {
        private class ThrStartObj
        {
            public int startCust;
            public int custCnt;
            public List<Customer> custList;
            public Mutex accCustList;
            public string[] source;
        }
        private readonly List<Customer> customers = new List<Customer>();
        private readonly string _dataFile;
        private int custForThr = 5000;
        private readonly int linesPerCust = 6;
        private readonly int linesSkipHead = 3;
        private readonly int linesSkipTail = 2;
        private readonly bool takeThrFromPool = false;
        private string[] source;

        public XmlRangeLoader(string dataFile, bool takeThrFromPool)
        {
            this._dataFile = dataFile;
            this.takeThrFromPool = takeThrFromPool;
        }

        /// <summary>
        /// skip first 3 lines xml header and open list tag
        /// 6 lines for customer
        // <Customer>
        //  <Id>2</Id>
        //  <FullName>Josefa Crona</FullName>
        //  <Email>JosefaCrona98 @yahoo.com</Email>
        //  <Phone>1-851-360-5889</Phone>
        //</Customer>
        /// </summary>
        public void LoadData()
        {
            var stw = new Stopwatch();
            stw.Start();
            var mt = new Mutex();
            var cl = new List<Customer>();
            var custCnt = (long)(File.ReadLines(_dataFile).Count() - linesSkipHead-linesSkipTail) / linesPerCust;
            if (custForThr > custCnt) custForThr = (int)custCnt;
            // supposed to be read only, so no sync context required
            //source.AddRange(File.ReadAllLines(_dataFile));
            source = new string[custCnt];
            source = File.ReadAllLines(_dataFile);
            
            var thrCnt = custCnt / custForThr;
            if (takeThrFromPool)
            {
                int workedThred = (int)thrCnt;
                using(ManualResetEvent resetEvent = new ManualResetEvent(false))
                {
                    for (int i = 0; i < thrCnt; i++)
                    {
                        var thrSO = new ThrStartObj()
                        {
                            startCust = i * custForThr,
                            custCnt = custForThr,
                            accCustList = mt,
                            custList = cl,
                            source = source
                        };
                        ThreadPool.QueueUserWorkItem(
                            new WaitCallback(x =>
                            {
                                RangeLoadWorker(thrSO);
                                if (Interlocked.Decrement(ref workedThred) == 0) resetEvent.Set();
                            }));
                    }
                    resetEvent.WaitOne();
                }
            }
            else
            {
                var thrList = new List<Thread>();
                for (int i = 0; i < thrCnt; i++)
                {
                    var thrSO = new ThrStartObj()
                    {
                        startCust = i * custForThr,
                        custCnt = custForThr,
                        accCustList = mt,
                        custList = cl,
                        source = source
                    };
                    var thr = new Thread(RangeLoadWorker);
                    thrList.Add(thr);
                    thr.Start(thrSO);
                }
                foreach (var thr in thrList) thr.Join();
            }
            stw.Stop();
            var methDescr = takeThrFromPool ? "pool" : "native";
            Console.WriteLine($"{methDescr}, колич. потоков - {thrCnt}, время на обр. файла - {stw.Elapsed.TotalMilliseconds} ms");
        } 

        private void RangeLoadWorker(object so)
        {
            if (!File.Exists(_dataFile)) return;
            List<string> custlines = new List<string>();
            custlines.Add("<Customers>");
            //custlines.AddRange(File.ReadLines(_dataFile)
            //    .Skip(((so as ThrStartObj).startCust) * linesPerCust + linesSkipHead)
            //    .Take((so as ThrStartObj).custCnt * linesPerCust)
            //    .ToList());
            //custlines.AddRange((so as ThrStartObj).source.((so as ThrStartObj).startCust * linesPerCust + linesSkipHead
            //, (so as ThrStartObj).custCnt * linesPerCust));
            int start = (so as ThrStartObj).startCust * linesPerCust + linesSkipHead;
            int end = (so as ThrStartObj).startCust * linesPerCust + linesSkipHead + (so as ThrStartObj).custCnt * linesPerCust;
            for (int i = start; i < end; i++) custlines.Add((so as ThrStartObj).source[i]);

            if (custlines[custlines.Count-1].Trim() != "</Customers>" ) custlines.Add("</Customers>"); // close tag at the file end
            if (custlines[custlines.Count - 2].Trim() == "</Customers>") custlines.RemoveAt(custlines.Count - 1); // second close tag at the file end
            var parser = new XmlParser(ref custlines);
            var custList = parser.Parse();
            var dwriter = new CustomerRepository();
            foreach (var customer in custList) dwriter.AddCustomer(customer);
            
            //if((so as ThrStartObj).accCustList.WaitOne())
            //{
            //    try
            //    {
            //        (so as ThrStartObj).custList.AddRange(custList);
            //    }
            //    finally 
            //    {
            //        (so as ThrStartObj).accCustList.ReleaseMutex();                        
            //    }
            //}
        }
        
    }
}

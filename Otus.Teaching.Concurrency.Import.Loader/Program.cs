﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using Microsoft.VisualBasic;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Otus.Teaching.Concurrency.Import.Loader.Loaders;

namespace Otus.Teaching.Concurrency.Import.Loader
{
    class Program
    {
        private static string _dataFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "customers.xml");
        private static int _generateCnt = 100000;
        
        static void Main(string[] args)
        {
            if (args != null && args.Length == 1)
            {
                _dataFilePath = args[0];
            }
            Console.Write("Create table customers...");
            var dbc = new DbCreator("Host=localhost;Port=5431;Username=postgres;Password=postgres;Database=postgres");
            dbc.createDB();
            Console.WriteLine("Choose generation method: E - external executable, I - current process method");
            var genMode = Console.ReadKey();
            Console.WriteLine($"Current process Id {Process.GetCurrentProcess().Id}...");
            if (genMode.KeyChar == 'E')
            {
                var sw = new Stopwatch();
                sw.Start();
                var genProcess = new Process();
                genProcess.StartInfo.FileName = @"F:\projects\otusPromoHW\ConcurencyHW\Otus.Teaching.Concurrency.Import\Otus.Teaching.Concurrency.Import.DataGenerator.App\bin\Debug\netcoreapp3.1\Otus.Teaching.Concurrency.Import.DataGenerator.App.exe";
                genProcess.StartInfo.Arguments = $"{_dataFilePath}  {_generateCnt.ToString()}";
                genProcess.Start();
                Console.WriteLine($"Fake Customer generator started with process Id {genProcess.Id}...");
                genProcess.WaitForExit();
                sw.Stop();
                Console.WriteLine($"generated in {sw.Elapsed.TotalMilliseconds} ms");
            }
            else
            {
                var sw = new Stopwatch();
                sw.Start();
                Console.WriteLine($"Fake Customer generator started with process Id {Process.GetCurrentProcess().Id}...");
                GenerateCustomersDataFile();
                sw.Stop();
                Console.WriteLine($"generated in {sw.Elapsed.TotalMilliseconds} ms");
            }

            Console.WriteLine($"Loader started with process Id {Process.GetCurrentProcess().Id}...");
            Console.WriteLine("using thread pool");
            var loader = new XmlRangeLoader(_dataFilePath, true);
            loader.LoadData();

            Console.WriteLine("using native threads");
            dbc.createDB();
            loader = new XmlRangeLoader(_dataFilePath, false);
            loader.LoadData();
        }

        static void GenerateCustomersDataFile()
        {
            var xmlGenerator = new XmlGenerator(_dataFilePath, _generateCnt);
            xmlGenerator.Generate();
        }
    }
}